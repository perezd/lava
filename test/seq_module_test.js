var assert = require('assert');
var seq    = require('modules/seq').load();


test("basic incr seq", function() {
  var incr = seq.incr();
  assert.equal(incr.next(), 0);
  assert.equal(incr.next(), 1);
  assert.equal(incr.next(), 2);
  assert.equal(incr.next(), 3);
  incr.reset();
  assert.equal(incr.next(), 0);
  assert.equal(incr.next(), 1);
});


test("basic decr seq", function() {
  var decr = seq.decr();
  assert.equal(decr.next(), 0);
  assert.equal(decr.next(), -1);
  assert.equal(decr.next(), -2);
  assert.equal(decr.next(), -3);
  decr.reset();
  assert.equal(decr.next(), 0);
  assert.equal(decr.next(), -1);
});


test("incr seq offset", function() {
  var decr = seq.incr(5);
  assert.equal(decr.next(), 5);
  assert.equal(decr.next(), 6);
  assert.equal(decr.next(), 7);
  assert.equal(decr.next(), 8);
  decr.reset();
  assert.equal(decr.next(), 5);
  assert.equal(decr.next(), 6);
});


test("decr seq offset", function() {
  var decr = seq.decr(5);
  assert.equal(decr.next(), 5);
  assert.equal(decr.next(), 4);
  assert.equal(decr.next(), 3);
  assert.equal(decr.next(), 2);
  decr.reset();
  assert.equal(decr.next(), 5);
  assert.equal(decr.next(), 4);
});


test("seq repeat", function() {
  var a = seq.repeat(seq.incr(), 3);
  assert.equal(a.next(), 0)
  assert.equal(a.next(), 1)
  assert.equal(a.next(), 2)
  assert.equal(a.next(), 0)
  assert.equal(a.next(), 1)
  assert.equal(a.next(), 2)
  assert.equal(a.next(), 0)
  assert.equal(a.next(), 1)
  a.reset();
  assert.equal(a.next(), 0)
});


test("seq prefetch", function() {
  var a = seq.incr();
  assert.deepEqual(a.next(5), [0,1,2,3,4]);
  assert.deepEqual(a.next(5), [5,6,7,8,9]);
  a.reset();
  assert.deepEqual(a.next(5), [0,1,2,3,4]);
});


test("seq repeater prefetch", function() {
  var a = seq.repeat(seq.incr(), 3);
  assert.deepEqual(a.next(5), [0,1,2,0,1]);
  assert.deepEqual(a.next(5), [2,0,1,2,0]);
});
