var _midi  = require('midi');
var assert = require('assert');

// returns an array of port name -> port
// number pairs, for a given connection.
var listMidiPorts = function(conn) {
  conn = midiPort(conn).port;
  assert(conn.getPortCount, "cannot get midi ports");
  var names = {};
  for (var i = conn.getPortCount() - 1; i >= 0; i--){
    names[conn.getPortName(i)] = i;
  };
  return names;
};

// opens a midi connection given a midi
// connection and a midi port number.
var openMidiPort = function(conn, portNum) {
  conn = midiPort(conn);
  if (conn.isOpen) {
    return false;
  } else {
    if (isNaN(Number(portNum))) {
      // sets up a virtual port if a string is provided.
      conn.port.openVirtualPort(portNum);
    } else {
      conn.port.openPort(Number(portNum));
    }
    conn.isOpen = true;
    return conn;
  }
};

// closes an already open midi connection.
var closeMidiPort = function(conn) {
  conn = midiPort(conn);
  if (!conn.isOpen) {
    return false;
  } else {
    conn.port.closePort();
    conn.isOpen = false;
    return conn;
  }
};

// sends a midi message to a midi connection.
var sendMidiMessage = function(conn, message) {
  conn = midiPort(conn);
  if (conn.isOpen) {
    conn.port.sendMessage(message);
    return true;
  } else {
    return false;
  }
};

var genPort = function(port) {
  var gen = function() { return { port: new port(), isOpen: false }; };
  gen._isPort = true;
  return gen;
};

var midiPort = function(object) {
  if (object._isPort) {
    return object();
  } else if (object.port) {
    return object;
  } else {
    assert.fail("object provided is not a midi port.");
  }
}

// creates a new midi instance, for public consumption.
exports.load = function() {

  // our root midi object
  var midi = { ports: {} };

  // midi message for turning a note on.
  midi.NOTE_ON  = 0x90;

  // midi message for turning a note off.
  midi.NOTE_OFF = 0x80;

  // convienence function generator for pressing a midi
  // note the way a human would. It assumes its likely coupled
  // with something like a looper module which instructs the
  // function what the previous value to turn OFF is.
  midi.note = function(port, velocity) {
    if (!velocity) {
      velocity = 100;
    }
    return function(value, prevValue) {
    if (prevValue) {
      midi.send(port, [midi.NOTE_OFF, prevValue, velocity]);
    }
      midi.send(port, [midi.NOTE_ON, value, velocity]);
    };
  };

  // create new input and output
  // midi ports for our object instance.
  // we wrap each port in a simple object so that
  // we can properly encapsulate any state.
  midi.ports.in  = genPort(_midi.input);
  midi.ports.out = genPort(_midi.output);

  // returns a list of midi ports for a given connection (in/out).
  midi.list = listMidiPorts;

  // accessor for the raw midi port.
  midi.port = function(conn) { return midiPort(conn).port };

  // opens a given midi connection (in/out).
  midi.open = openMidiPort;

  // sends a midi message given a connection.
  midi.send = sendMidiMessage;

  // closes a given midi connection (in/out).
  midi.close = closeMidiPort;

  return midi;
};
