# Lava. Generative Music REPL.

## Getting Started

boot up the REPL using the following command:

```
./bin/repl
```

You'll see output like this:

```
* 1352445797498 [REPL]: loading module - loop
* 1352445797501 [REPL]: loading module - seq
>>>
```
The REPL comes with a number of builtin modules, we'll review them below.

## Seq

The seq module is used to create arbitrary sequences of numbers.

### seq.do

seq.do allows you to define an arbitrary numeric sequence, using a function.

```
>>> mult = seq.do(function(i) { return i * 2; });
>>> mult.next() // -> 0
>>> mult.next() // -> 2
>>> mult.next() // -> 4
>>> mult.next() // -> 6
```

sequences are resetable.

```
>>> mult.reset()
>>> mult.next() // -> 0
>>> mult.next() // -> 2
```

sequences can be eagerly executed, if you want that.

```
>>> incr = seq.do(function(i) { return i + 1; });
>>> incr.next(5) -> [0,1,2,3,4]
>>> incr.next(5) -> [5,6,7,8,9]
```

### seq.incr

because infinitely incrementing sequences are handy, we do that for you.

```
>>> incr = seq.incr();
>>> incr.next() // -> 0
>>> incr.next() // -> 1
```

its possible to start the incrementer at an offset, as well.

```
>>> incr = seq.incr(10);
>>> incr.next() // -> 10;
>>> incr.next() // -> 11;
>>> incr.next() // -> 12;
```

### seq.decr

because infinitely decrementing sequences are handy, we do that for you.

```
>>> decr = seq.decr();
>>> decr.next() // -> 0
>>> decr.next() // -> -1
```

its possible to start the decrementer at an offset, as well.

```
>>> decr = seq.decr(10);
>>> decr.next() // -> 10;
>>> decr.next() // -> 9;
>>> decr.next() // -> 8;
```

### seq.repeat

sequences are conceptually infinite, but sometimes you want your sequence to repeat at a given length.

```
>>> rep = seq.repeat(seq.incr(), 3);
>>> rep.next() // -> 0
>>> rep.next() // -> 1
>>> rep.next() // -> 2
>>> rep.next() // -> 0
>>> rep.next() // -> 1
```

repeats still support reset as expected, and also support eager fetching:

```
>>> rep = seq.repeat(seq.incr(), 3);
>>> rep.next(5) // -> [0,1,2,0,1]
```

## Midi

The midi module enables you to interact with midi devices within the REPL.

### midi.list

returns a name->port map for a given midi connection.

```
>>> midi.list(midi.ports.out) // -> { "Motu Express": 0 }
```
The numeric values are needed if you intend to open a midi port.

### midi.open

opens a midi port given a port and its port number.

```
>>> mOut = midi.open(midi.ports.out, 0); // opens up a midi output port.
```

If you'd like to use a "virtual port", pass a string as the second argument.

```
>>> mOut = midi.open(midi.ports.out, "some port"); // a midi port object
```

### midi.send

sends a message to an open midi port.

```
>>> midi.send(mOut, [144, 52, 20]); // true
```

### midi.close

when you're done with a midi session, its good to close it.

```
>>> midi.close(mOut); // true
```

## Loop

The loop module provides interval-based sequence processing, using callbacks.

### loop.do

```
>>> loop.do(500, seq.repeat(seq.incr(5), 5), console.log).start();
0 undefined
1 0
2 1
3 2
...
```

what you'll notice is that the loop provides the current AND previous values to the callback. This allows the callback to decide to send a `midi.NOTE_OFF` message to stop playing a previous note.

`loop.do` returns an object with a basic API for you to manipulate the loop, dynamically.

#### loop.start()

starts up the loop, its stopped on create and must be started manually.

#### loop.stop()

stops an already started loop.

#### loop.restart(interval)

restarts a loop, and takes an optional interval, this will replace the speed of the loop provided to the initial constructor.

#### loop.add(seq)

dynamically add a new sequence to the loop. Loop intervals can be considered polyphonic, if an array of sequences is detected. NOTE: this does not restart the loop.

#### loop.clear()

clears all sequences found in the loop, but does not stop it.

---

Its common to want to integrate the loop with the midi module, and its very easy to do so:

```
>>> loop.do(300,
		   [seq.repeat(seq.incr(50), 5), seq.repeat(seq.incr(60), 5)],
		   midi.note(midi.open(midi.ports.out, 'lava'))).start()
```

This example will create a loop at 300ms, playing 2 notes (one from each sequence in the array), and will pipe it to our virtual midi output port named 'lava'.
