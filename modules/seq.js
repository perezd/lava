
/* primitves */

// the raw sequence object.
// supports a prefetch of values via next.
var doSeq = function(value, f) {
  var v, value;
  if (typeof value === 'function' && !f) {
    f = value; v = 0; value = 0;
  } else {
    v = value;
  }

  // returns a single value.
  var singleNext = function() {
    var ov = v;
    v = f(v);
    return ov;
  };

  // returns many values as an array.
  var multiNext = function(i) {
    var values = [];
    while(i--) {
      values.push(singleNext());
    }
    return values;
  };

  return {
    next  : function(i) { return i ? multiNext(i) : singleNext(); },
    reset : function()  { v = value; }
  };
};

// fetches a sequence to a certain bound, and then repeats.
// supports a prefetch of values via next.
var repeatSeq = function(seq, limit) {
  var pos = 0;

  // returns a single value and respects the repeater.
  var singleNext = function() {
    var nv = seq.next();
    if (++pos === limit) { seq.reset(); pos = 0; }
    return nv;
  };

  // returns an array of values and respects the repeater.
  var multiNext = function(i) {
    var values = [];
    while(i--) {
      values.push(singleNext());
    }
    return values;
  };

  return {
    next  : function(i) { return i ? multiNext(i) : singleNext(); },
    reset : function()  { seq.reset(); pos = 0; }
  };
};

/* convenience functions */

// an infinitely incrementing sequence.
var incrSeq = function(start) {
  return doSeq((start||0), function(i) { return i + 1; });
};

// an infintely decrementing sequence.
var decrSeq = function(start) {
  return doSeq((start||0), function(i) { return i - 1; });
};

// basic lazy sequence generator.
exports.load = function() {
  return {
    // our basic seq primitive.
    do: doSeq,

    // basic incrementing seq.
    incr: incrSeq,

    // basic decrementing seq.
    decr: decrSeq,

    // seq repeater.
    repeat: repeatSeq
  };
};
