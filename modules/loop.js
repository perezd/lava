
var seqLoop = function(interval, seq, apply) {

  // apply is the function that receives the values that the loop
  // returns from its sequences at every given iteration.
  if (!apply) {
    apply = function() {};
  }

  // seq supports an array of sequences to be played
  // polyphonically at the same interval.
  if (!Array.isArray(seq)) {
    seq = [seq];
  }
  var _loop;

  var startLoop = function(i) {
    if (_loop != null) {
      return;
    }
    _loop = setInterval(function() {
      seq.forEach(function(s) {
        if (!s.next) { return; }
        var nextValue = s.next();
        apply(nextValue, s._prevValue);
        s._prevValue = nextValue;
      });
    }, i ? i : interval);
  };

  var stopLoop = function() {
    if (_loop == null) {
      return;
    }
    clearInterval(_loop);
    _loop = null;
    seq.forEach(function(s) { s.reset(); });
  };

  return {

    // dynamically add a new sequence to the
    // looper, will start processing it immediately
    // if its running.
    add: function(s) {
      seq.push(s);
    },

    // clear all sequences, but do not stop looping.
    clear: function() {
      seq = [];
    },

    // stops the looper and resets all sequences.
    stop: stopLoop,

    // starts the looper.
    start: startLoop,

    // restarts the loop, allows the caller to provide
    // a new interval speed, on restart.
    restart: function(interval) {
      stopLoop();
      startLoop(interval);
    }
  };
};

// the loop module.
exports.load = function(lava) {
  return {
    do: seqLoop
  };
};
